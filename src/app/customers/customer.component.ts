import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormArray} from '@angular/forms';
import {Customer} from './customer';
import {debounceTime} from 'rxjs/operators';
import {Validations} from '../validations/Validations';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customerForm: FormGroup;
  customer = new Customer();
  emailMessage: string;
  addresses: FormArray;
  constructor(private formBuilder: FormBuilder) {
  }

  private validationMessages = {
    required: 'Please enter your email address',
    email: 'Please entere a valid email  address'
  };


  ngOnInit() {
    this.addresses =this.formBuilder.array([this.buildAddress()]);
    this.customerForm = this.formBuilder.group({
      'firstName': ['', [Validators.required, Validators.minLength(4)]],
      'lastName': ['', [Validators.required, Validators.maxLength(50)]],
      emailGroup: this.formBuilder.group({
        'email': ['', [Validators.required, Validators.email]],
        'confirmEmail': ['', [Validators.required, Validators.email]]
      }, {validator: Validations.emailMatcher}),
      phone: '',
      notification: 'email',
      rating: [null, Validations.ratingRange(1, 5)],
      'sendCatalog': true,
      addresses: this.addresses
    });


    this.customerForm.get('notification').valueChanges.subscribe(value => this.setNotification(value));

    const emailControl = this.customerForm.get('emailGroup.email');
    emailControl.valueChanges.pipe(debounceTime(1000)).subscribe(value => this.setMessage(emailControl));
  }

  buildAddress(): FormGroup {
    return this.formBuilder.group({
      addressType: 'home',
      street1: '',
      street2: '',
      city: '',
      state: '',
      zip: ''
    });
  }

  getAddress(): FormArray {
    return <FormArray>this.customerForm.get('addresses');
  }

  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }

  save() {
    console.log(this.customerForm);
    console.log('Saved: ' + JSON.stringify(this.customerForm.value));
  }

  populateTestData(): void {
    // can set partial object values
    this.customerForm.patchValue({
      firstName: 'John',
      lastName: 'Arbuckles',
      sendCatalog: false
    });

    // must set all object values
    this.customerForm.setValue({
      firstName: 'John',
      lastName: 'Arbuckles',
      email: 'john@arbuclkes.com',
      sendCatalog: false
    });
  }

  setNotification(notifyVia: string): void {
    const phoneControl = this.customerForm.get('phone');
    if (notifyVia === 'text') {
      phoneControl.setValidators(Validators.required);
    } else {
      phoneControl.clearValidators();
    }
    phoneControl.updateValueAndValidity();

  }

  private setMessage(c: AbstractControl) {
    this.emailMessage = '';
    if ((c.touched || c.dirty) && c.errors) {
      this.emailMessage = Object.keys(c.errors).map(key => this.emailMessage += this.validationMessages[key]).join(' ');
    }
  }
}

